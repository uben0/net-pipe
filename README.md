# net-pipe

Easy to use file sharing tool using encryption layer.
It uses multicast on the local network to connect sending and receiving sides.

## Exemples
 ```
# sending side
$ np <secret-key> send <file-path>

# receiving side
$ np <secret-key> recv <file-path>
 ```
