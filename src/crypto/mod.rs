use chacha20poly1305::{
    aead::{AeadInPlace, NewAead},
    ChaCha20Poly1305,
};
use std::io::{self, Read, Write};
mod block_io;
use block_io::{BlockReader, BlockWriter};

pub struct Key {
    bytes: [u8; 32],
}
impl std::convert::From<&[u8]> for Key {
    fn from(mut slice: &[u8]) -> Self {
        let mut bytes = [0; 32];
        if slice.len() > 32 {
            slice = &slice[..32];
        }
        bytes[..slice.len()].copy_from_slice(slice);
        Self{bytes}
    }
}
impl std::convert::From<&str> for Key {
    fn from(s: &str) -> Self {
        s.as_bytes().into()
    }
}

struct Nonce {
    id: u32,
    n: u64,
}
impl Nonce {
    fn new(id: u32) -> Self {
        Self { id, n: 0 }
    }
}
impl Iterator for Nonce {
    type Item = [u8; 12];
    fn next(&mut self) -> Option<Self::Item> {
        let mut result = [0; 12];
        result[0..4].copy_from_slice(&self.id.to_be_bytes());
        result[4..12].copy_from_slice(&self.n.to_be_bytes());
        self.n = self.n.checked_add(1)?;
        Some(result)
    }
}

pub struct Encrypt<O: Write> {
    output: BlockWriter<O>,
    cypher: ChaCha20Poly1305,
    nonce: Nonce,
}
impl<O: Write> Encrypt<O> {
    pub fn new(output: O, key: &Key) -> Self {
        Self{
            output: BlockWriter::new(output),
            nonce: Nonce::new(0),
            cypher: ChaCha20Poly1305::new(&chacha20poly1305::Key::from_slice(&key.bytes)),
        }
    }
    pub fn send(&mut self, buf: &mut Vec<u8>) -> std::io::Result<()> {
        if let Some(nonce) = self.nonce.next() {
            self.cypher
                .encrypt_in_place(&nonce.into(), &[], buf)
                .map_err(|_| io::Error::from(io::ErrorKind::InvalidInput))?;
            self.output.write_all(buf.as_ref())
        } else {
            Err(io::Error::from(io::ErrorKind::InvalidInput))
        }
    }
}
pub struct EncryptWriter<O: Write> {
    output: Encrypt<O>,
    buf: Vec<u8>,
}
impl<O: Write> Write for EncryptWriter<O> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.buf.clear();
        self.buf.extend_from_slice(buf);
        self.output.send(&mut self.buf)?;
        Ok(buf.len())
    }
    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}
impl<O: Write> std::convert::From<Encrypt<O>> for EncryptWriter<O> {
    fn from(output: Encrypt<O>) -> Self {
        Self{
            output, buf: vec![],
        }
    }
}

pub struct Decrypt<I: Read> {
    input: BlockReader<I>,
    cypher: ChaCha20Poly1305,
    nonce: Nonce,
}
impl<I: Read> Decrypt<I> {
    pub fn new(input: I, key: &Key) -> Self {
        Self{
            input: BlockReader::new(input),
            nonce: Nonce::new(0),
            cypher: ChaCha20Poly1305::new(&chacha20poly1305::Key::from_slice(&key.bytes)),
        }
    }
    pub fn recv(&mut self, buf: &mut Vec<u8>) -> std::io::Result<usize> {
        if self.input.read_block(buf)? > 0 {
            if let Some(nonce) = self.nonce.next() {
                self.cypher
                    .decrypt_in_place(&nonce.into(), b"", buf)
                    .map_err(|_| io::Error::from(io::ErrorKind::InvalidInput))?;
                Ok(buf.len())
            } else {
                Err(io::Error::from(io::ErrorKind::InvalidInput))
            }
        } else {
            Ok(0)
        }
    }
}
pub struct DecryptReader<I: Read> {
    input: Decrypt<I>,
    buf: Vec<u8>,
}
impl<I: Read> Read for DecryptReader<I> {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        self.input.recv(&mut self.buf)?;
        if self.buf.len() <= buf.len() {
            buf[..self.buf.len()].copy_from_slice(&self.buf);
            Ok(self.buf.len())
        } else {
            Err(io::Error::from(io::ErrorKind::InvalidInput))
        }
    }
}
impl<I: Read> std::convert::From<Decrypt<I>> for DecryptReader<I> {
    fn from(input: Decrypt<I>) -> Self {
        Self{
            input, buf: vec![],
        }
    }
}
