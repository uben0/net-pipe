use std::io::{self, Read, Write, BufReader, BufWriter};

pub struct BlockReader<I: Read> {
    input: BufReader<I>,
}
impl<I: Read> BlockReader<I> {
    pub fn new(input: I) -> Self {
        Self{input: BufReader::new(input)}
    }
    pub fn read_block(&mut self, buf: &mut Vec<u8>) -> io::Result<usize> {
        buf.clear();
        let mut size = [0; 2];
        match self.input.read_exact(&mut size) {
            Ok(()) => {
                let size = u16::from_be_bytes(size) as usize;
                buf.resize(size, 0);
                self.input.read_exact(buf)?;
                Ok(size)
            }
            Err(e) => {
                if e.kind() == io::ErrorKind::UnexpectedEof {
                    Ok(0)
                } else {
                    Err(e)
                }
            }
        }
    }
}

pub struct BlockWriter<O: Write> {
    output: BufWriter<O>,
}
impl<O: Write> BlockWriter<O> {
    pub fn new(output: O) -> Self {
        Self{output: BufWriter::new(output)}
    }
}
impl<O: Write> Write for BlockWriter<O> {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        let size = (buf.len() as u16).to_be_bytes();
        self.output.write_all(&size)?;
        self.output.write_all(buf)?;
        self.flush()?;
        Ok(buf.len())
    }
    fn flush(&mut self) -> io::Result<()> {
        self.output.flush()
    }
}