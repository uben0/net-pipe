use std::net::{Ipv6Addr, SocketAddrV6, TcpListener, TcpStream, UdpSocket};
use structopt::StructOpt;

mod crypto;

const SEND_HUB_ADDR: &str = "[ff12::9513:cd14:cebb:7a2a]:48721";
const RECV_HUB_ADDR: &str = "[ff12::9513:cd14:cebb:7a2b]:48722";

fn hub_handshake(src: SocketAddrV6, dst: SocketAddrV6) -> std::io::Result<TcpStream> {
    let listener = TcpListener::bind("[::]:0")?;
    let multicast = UdpSocket::bind((Ipv6Addr::UNSPECIFIED, src.port()))?;
    multicast.join_multicast_v6(&src.ip(), 0)?;
    multicast.send_to(&listener.local_addr()?.port().to_be_bytes(), &dst)?;
    let mut buffer = [0; 2];
    let (size, addr) = multicast.recv_from(&mut buffer)?;
    if size != 2 {
        return Err(std::io::ErrorKind::InvalidData.into());
    }
    multicast.leave_multicast_v6(src.ip(), 0)?;
    multicast.send_to(&[0, 0], &addr)?;
    let port: u16 = u16::from_be_bytes(buffer);
    if port == 0 {
        Ok(listener.accept()?.0)
    } else {
        std::mem::drop(listener);
        let mut addr = addr;
        addr.set_port(port);
        TcpStream::connect(&addr)
    }
}

// fn connect_via_hub(hub_addr: SocketAddrV6) -> std::io::Result<TcpStream> {
//     let listener = TcpListener::bind("[::]:0")?;
//     let handshake = UdpSocket::bind("[::]:0")?;
//     handshake.send_to(&listener.local_addr()?.port().to_be_bytes(), &hub_addr)?;
//     std::mem::drop(handshake);
//     Ok(listener.accept()?.0)
// }

// fn listen_via_hub(hub_addr: SocketAddrV6) -> std::io::Result<TcpStream> {
//     let multicast_waiter = UdpSocket::bind((Ipv6Addr::UNSPECIFIED, hub_addr.port()))?;
//     multicast_waiter.join_multicast_v6(&hub_addr.ip(), 0)?;
//     let mut buffer = [0; 2];
//     let (size, mut addr) = multicast_waiter.recv_from(&mut buffer)?;
//     if size != 2 {
//         return Err(std::io::ErrorKind::InvalidData.into());
//     }
//     let port: u16 = u16::from_be_bytes(buffer);
//     multicast_waiter.leave_multicast_v6(hub_addr.ip(), 0)?;
//     std::mem::drop(multicast_waiter);
//     addr.set_port(port);
//     TcpStream::connect(addr)
// }

#[derive(StructOpt)]
struct Args {
    /// Encryption key
    key: String,
    #[structopt(subcommand)]
    cmd: Cmd,
}

#[derive(StructOpt)]
enum Cmd {
    /// Send data to the receiving pair
    Send { input_file: Option<String> },
    /// Receive data from the sending pair
    Recv { output_file: Option<String> },
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args = Args::from_args();
    let key: crypto::Key = args.key.as_str().into();
    match args.cmd {
        Cmd::Send { input_file } => {
            let stdin_lock = std::io::stdin();
            let mut input_stream: Box<dyn std::io::Read> = if let Some(path) = input_file {
                Box::new(std::fs::File::open(path)?)
            } else {
                Box::new(stdin_lock.lock())
            };
            let output_stream = hub_handshake(SEND_HUB_ADDR.parse().unwrap(), RECV_HUB_ADDR.parse().unwrap())?;
            let mut output_stream: crypto::EncryptWriter<_> =
                crypto::Encrypt::new(output_stream, &key).into();
            std::io::copy(&mut input_stream, &mut output_stream)?;
            Ok(())
        }
        Cmd::Recv { output_file } => {
            let stdout_lock = std::io::stdout();
            let mut output_stream: Box<dyn std::io::Write> = if let Some(path) = output_file {
                Box::new(std::fs::File::create(path)?)
            } else {
                Box::new(stdout_lock.lock())
            };
            let input_stream = hub_handshake(RECV_HUB_ADDR.parse().unwrap(), SEND_HUB_ADDR.parse().unwrap())?;
            let mut input_stream: crypto::DecryptReader<_> =
                crypto::Decrypt::new(input_stream, &key).into();
            std::io::copy(&mut input_stream, &mut output_stream)?;
            Ok(())
        }
    }
}
